# -*- coding: UTF-8 -*-

import tornado.ioloop
import tornado.web
import wulai_logger as logger
import sys
import json
from utils import FLAGS
import webhook_handler as webhook


class WebhookResponseHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.write("It works")
    def post(self):
        logger.info('<<<<<<<<<<<<<<<<<<<<<< in WebhookResponseHandler <<<<<<<<<<<<<<<<<<<<<<')
        params = json.loads(self.request.body)
        logger.info('webhook input params=%s' % json.dumps(params))
        query = params['msg_body']['text']['content']
        user_id = params['user_id']
        group_user_id = 0
        if 'group_info' in params and 'from_group_user' in params['group_info']:
            group_user_id = params['group_info']['from_group_user']
        responses = params['bot_response']
        response_list = []
        rsp = dict()
        rsp['suggested_response'] = response_list
        try:
            params = dict()
            params['query'] = query
            params['user_id'] = user_id
            params['group_user_id'] = group_user_id
            params['responses'] = responses
            response_list = webhook.get_reply(params)
            rsp['suggested_response'] = response_list
            logger.info('response=%s' % json.dumps(rsp))
            self.set_header('Access-Control-Allow-Origin', '*')
            self.set_header('Content-Type', 'application/json; charset=UTF-8')
            self.finish(json.dumps(rsp))
        except Exception as e:
            logger.error('get_response error errmsg=%s' % (e), exc_info=True)
            self.set_header('Access-Control-Allow-Origin', '*')
            self.set_header('Content-Type', 'application/json; charset=UTF-8')
            self.write(rsp)
    logger.info('>>>>>>>>>>>>>>>>>>>>>>>> leave WebhookResponseHandler >>>>>>>>>>>>>>>>>>>>>>>>')


# class SendHandler(tornado.web.RequestHandler):
#     def get(self, *args, **kwargs):
#         self.write("It works")

#     def post(self, *args, **kwargs):
#         logger.info(self.request.body)
#         self.write("todo ...")


def make_app():
    return tornado.web.Application([
        (r"/v1/webhook", WebhookResponseHandler),
        # (r"/v1/send", SendHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    port = FLAGS.port
    app.listen(port)
    tornado.ioloop.IOLoop.current().start()
