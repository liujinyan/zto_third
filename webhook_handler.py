# -*- coding: UTF-8 -*-
import wulai_logger as logger
import json
import redis
from copy import deepcopy
from utils import CONFIG, _redis_inst
import requests
import  json


import re



def wuliu_type(url,danhao,type):
    url = "http://japi.zto.cn/logisticsTrace"

    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    d = {"msg_type": "BILLCODE_TRACE",
     "data": json.dumps({"billCode": "217866049801"})}
    r = requests.post(url=url,headers=headers,data=d)
    wuliu_type = r.json()['result']['scanType']
    return wuliu_type


def wangdian_tel(url,danhao,type):
    url = url

    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    d = {"msg_type": "BILLCODE_TRACE",
     "data": json.dumps({"billCode": danhao,'type':type})}
    r = requests.post(url=url,headers=headers,data=d)
    text = r.json()['result']['readText']

    print(text)
    ret_greed = re.findall(r'(\d+.\d+)', text)

    return ret_greed


url = "http://japi.zto.cn/logisticsTrace"
danhao = "217866049801"
type1 = '已签收'




def get_entity_ref(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity
    except:
        pass
    return None

def add_entity(one_suggested_response, entity_name, value, state=1):
    entities=one_suggested_response["detail"]["task"]["entities"]
    if get_entity_ref(entities, entity_name):
        get_entity_ref(entities, entity_name)["value"]=value
        get_entity_ref(entities, entity_name)["seg_value"]=value
    else:
        entities.append({
                            "name":entity_name,
                            "value":value,
                            "seg_value":value
                        })
    one_suggested_response["detail"]["task"]["state"]=state





def get_entity_value(entities, entity_name):
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity["value"]
    except:
        return ""
    return ""




def change_flow(one_suggested_response, 
                next_reply, next_action="", 
                entity_name=None, value_for_entity="", 
                last_entity_name=None,
                kill_task=False,
                state=1):
    """
    这里用于实现非正常流程跳转，也就是在webhook里面通过判断机器人做跳转
    参数说明：
        one_response
            对params["responses"]["suggested_response"] 任一元素的引用
        next_action
            希望下一个跳到哪哥action
        next_reply
            展示给用户的话术
        entity_name
            跳转需要修改的实体名称
        value_for_entity
            跳转需要修改的实体名称对应的实体值
            默认填空（“”）
    """

    if not kill_task and (not entity_name):
        logger.info("kill_task=False时entity_name必须是字符串并且在entities列表里面出现")
        return False

    try:
        if isinstance(next_reply, str):
            one_suggested_response["response"]=[one_suggested_response["response"][0]]
            first_answer=one_suggested_response["response"][0]
            first_answer["msg_body"]["text"]["content"]=next_reply
            first_answer["show_content"]=next_reply
        else:
            new_ans=one_suggested_response["response"][0]
            one_suggested_response["response"]=[]
            for one in next_reply:
                new_ans=deepcopy(new_ans)
                new_ans["msg_body"]["text"]["content"]=one
                new_ans["show_content"]=one
                new_ans["delay_ts"]+=1
                
                one_suggested_response["response"].append(new_ans)

        source=one_suggested_response["source"]
        if source != "TASK_BOT":
            return True
            
        entities=one_suggested_response["detail"]["task"]["entities"]
        if kill_task:
            kill_task_entities=[]
            kill_task_entities.append(get_entity_ref(entities, "app_id"))
            kill_task_entities.append(get_entity_ref(entities, "task_id"))
            one_suggested_response["detail"]["task"]["entities"]=kill_task_entities
            logger.info("提前终结任务：%s" % json.dumps(one_suggested_response["detail"]["task"]["entities"]))
        else:
            one_suggested_response["detail"]["task"]["action"]=next_action
            entity_ref=get_entity_ref(entities, entity_name)
            if entity_ref:
                logger.info("%s关联到的实体：%s" % (entity_name, json.dumps(entity_ref)))
                entity_ref["seg_value"]=value_for_entity
                entity_ref["value"]=value_for_entity
            else:
                logger.info("未关联到实体，增加%s=%s" % (entity_name, value_for_entity))
                new_entity={"name":entity_name,"value":value_for_entity,"seg_value":value_for_entity}
                entities.append(new_entity)

        

        if last_entity_name:
            new_entity={"name":"last_entity_name","value":last_entity_name,"seg_value":last_entity_name}
            entities.append(new_entity)
            logger.info("下一个实体应该是：%s" % last_entity_name)
        one_suggested_response["detail"]["task"]["state"]=state
    except:
        logger.error("webhook内部变更出现异常", exc_info=True)
        return False
    return True

def get_reply(params):
    logger.info("get_reply start "+">"*50)
    # 获取基本信息
    user_id=params["user_id"]
    query=params["query"]
    for one_response in params["responses"]["suggested_response"]:
        try:
            source=one_response["source"]
        except:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":
            # 是机器人
            logger.info("召回任务机器人")
            logger.info("get_reply 传入数据：params=%s" % json.dumps(params))
            
            # 获取实体列表引用和action的值
            entities=one_response["detail"]["task"]["entities"]
            action=one_respon ["tail"]["task"]["action"]

            if action == "zto_state" and int(get_entity_value(entities, "interval")) > 1:
                # TODO
                cur_entity_name="entity_zto_state"
                cur_entity_value= wuliu_type(url,danhao,type1)
                next_reply=wangdian_tel(url,danhao,type1) 
                if  cur_entity_value=="签收"：
                    next_action="zto_recive"
                    last_entity_name="entity_zto_solveornot"
                    change_flow(one_response, next_reply,next_action, cur_entity_name, cur_entity_value)

                else ：
                    next_action="zeo_havesolve"
                    last_entity_name="entity_zto_solveornot"
                    change_flow(one_response, next_reply,next_action, cur_entity_name, cur_entity_value)

            
            #if action == "zto_receive_name" and int(get_entity_value(entities, "interval")) > 1：





            # if action == "zto_ask_detail" and int(get_entity_value(entities, "interval")) > 1：
            #     cur_entity_name="entity_zto_complain"



            if action == "zto_change" and int(get_entity_value(entities, "interval")) > 1：
                cur_entity_name="entity_zto_com_kind"
                cur_entity_value = get_entity_value(entities, cur_entity_name)
                ASK_NAME = 1
                ASK_PHONE= 2
                ASK_complain =3
                if cur_entity_value == ASK_NAME  
                    next_reply="请重新输入收件人姓名："
                    next_action ="zto_receive_name"
                    last_entity_name = "entity_zto_receive_na"
                    add_entity(one_response, last_entity_name, "")
                    change_flow(one_response, next_reply, 
                        next_action, last_entity_name=last_entity_name)
                elif cur_entity_value ==ASK_PHONE
                    next_reply="请重新输入收件人电话："
                    next_action ="zto_recipant"
                    last_entity_name = reseive_tele
                    add_entity(one_response, last_entity_name, "")
                    change_flow(one_response, next_reply, 
                        next_action, last_entity_name=last_entity_name)
                else cur_entity_value == ASK_complain
                    next_reply="请输入详细描述："
                    next_action ="zto_ask_detail"
                    last_entity_name = reseive_tele
                    add_entity(one_response, last_entity_name, "")
                    change_flow(one_response, next_reply, 
                        next_action, last_entity_name=last_entity_name)
            if action == "另外一个需要特殊处理的别名，必须你可能需要访问数据库？读取redis？，你看着办":
                # TODO
                pass
                

        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")

    logger.info("get_reply end "+"<"*50)

    return params["responses"]["suggested_response"]
