certifi==2018.1.18
chardet==3.0.4

future==0.16.0
idna==2.6
numpy==1.14.5
redis==2.10.6
requests==2.18.4
tornado==5.0.2
urllib3==1.22
